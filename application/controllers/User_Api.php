
<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/API_Controller.php';
class User_Api extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function simple_api()
    {

        header("Access-Control-Allow-Origin: *");

        // API CONFIG
        $this->_apiConfig(array(
            'methods' => array('POST', 'GET')
        ));

    }

    public function api_limit()
    {
        /**
         * API Limit
         * ----------------------------------
         * @param: {int} API limit Number
         * @param: {string} API limit Type (IP)
         * @param: {int} API limit Time [minute]
         */

        $this->_APIConfig(array(
            // number limit, type limit, time limit (last minute)
            'limit' => array(50, 'ip', 'everyday'),
        ));
    }

    public function api_key(){
        /**
         * Use API Key without Database
         * ---------------------------------------------------------
         * @param: {string} Types
         * @param: {string} API Key
         */
        $this->_APIConfig(array(
            'methods' => array('POST'),
            //'key' => array('header', '13d1266d-81bd-4dc4-920f-eab672a802df'),

            // API Key With Database
            'key' => array('header'),

            // Add Custom data in API Response
            'data' => array(
                'is_login' => false
            ),

        ));

        // Data
        $data = 0;

        /**
         * Return API Response
         * ---------------------------------------------------------
         * @param: API Data
         * @param: Request Status Code
         */
        if (!empty($data)){
            $this->api_return($data, '200');
        }else{
            $this->api_return(array('status' => false, 'error' => 'Invalid Data'), '404');
        }

    }

    public function login(){
        header("Access-Control-Allow-Origin: *");

        // API Configuration
        $this->_apiConfig(array(
            'methods' => array('POST'),
        ));

        // you user authentication code will go here, you can compare the user with the database or whatever
        $payload = array(
            'id' => "Your User's ID",
            'other' => "Some other data",
        );

        // Load Authorization Library or Load in autoload config file
        $this->load->library('authorization_token');

        // generate a token
        $token = $this->authorization_token->generateToken($payload);

        // return data
        $this->api_return(
            array(
                'status' => true,
                "result" => array(
                    'token' => $token,
                ),

            ),
            200);
    }

    /**
     * view method
     *
     * @link [api/user/view]
     * @method POST
     * @return Response|void
     */
    public function view()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration [Return Array: User Token Data]
        $user_data = $this->_apiConfig(array(
            'methods' => array('POST'),
            'requireAuthorization' => true,
        ));

        //die(var_dump($user_data));

        // return data
        $this->api_return(
            array(
                'status' => true,
                "result" => array(
                    'user_data' => $user_data['token_data']
                ),
            ),
            200);
    }

    public function movies() {

        header('Access-Control-Allow-Origin: *');

        $this->_apiConfig(
            array(
                'methods' => array('GET', 'POST', 'DELETE'),
                'requireAuthorization' => true
            )
        );
        // On récupère le type de requête envoyé par le Client
        $requestType = $this->input->server('REQUEST_METHOD');

        // En fonction du type de requête
        switch ($requestType) {

            case 'GET' : $movies = $this->db->select('*')
                ->from('movies')
                ->get()
                ->result();
            $this->api_return($movies, '200');
            break;

            case 'POST' :

                $id = $this->uri->segment(4);

                if ($id != null) {
                    $data['id'] = $id;
                    $data['mov_name'] = $this->input->post('mov_name');
                    $data['mov_year'] = $this->input->post('mov_year');
                    $data['mov_img'] = $this->input->post('mov_img');
                    $data['mov_rating'] = $this->input->post('mov_rating');
                    $data['mov_link'] = $this->input->post('mov_link');
                    $data['mov_description'] = $this->input->post('mov_description');

                    if ($data != null) {
                        $this->db->where('id', $id)
                            ->update('movies', $data);

                        if ($this->db->affected_rows() > 0) {
                            $this->api_return(array('success' => 'Votre film a été modifié avec succès'), '200');
                        } else {
                            $this->api_return(array('error' , 'Une erreur est survenue lors de la modification du Film'), '404');
                        }

                    }
                } else {
                    $data['mov_name'] = $this->input->post('mov_name');
                    $data['mov_year'] = $this->input->post('mov_year');
                    $data['mov_img'] = $this->input->post('mov_img');
                    $data['mov_rating'] = $this->input->post('mov_rating');
                    $data['mov_link'] = $this->input->post('mov_link');
                    $data['mov_description'] = $this->input->post('mov_description');

                    if ($data != null) {

                        $this->db->insert('movies', $data);

                        if ($this->db->affected_rows() > 0) {
                            $this->api_return(array('success' => 'Votre film a été ajouté avec succès'), '200');
                        } else {
                            $this->api_return(array('error' , 'Une erreur est survenue lors de l\'ajout du Film'), '404');
                        }

                    }
                }
                break;

            case 'DELETE' :

                $id = $this->uri->segment(4);

                $checkIfIDExist = $this->db->select('id')
                    ->from('movies')
                    ->where('id', $id)
                    ->get()
                    ->row();

                //die(var_dump($checkIfIDExist));

                if ($checkIfIDExist) {
                    $this->db->where('id', $id)
                        ->delete('movies');

                    $this->api_return(array(
                        'success' => 'Le film numéro : ' . $id . ' a été supprimé'
                    ), '200');
                } else {
                    $this->api_return(array(
                       'error' => 'Le numéro du film (' . $id . ') n\'existe pas.'
                    ), '400');
                }
                break;
            case 'PUT' :
                //die(var_dump($_POST));

                //die(var_dump($id));

                //$this->api_return($this->input->post(), '200' );
                //die(var_dump($this->input->post()));
        }
    }
}
